<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WarBoard_TogglerPure" version="0.1" date="30/7/2009" >
		<Author name="Computerpunk" email="teromario@yahoo.com" />
		<Description text="Pure Toggler module created with LibWBToggler." />
		<VersionSettings gameVersion="1.3.3" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibWBToggler" />
			<Dependency name="Pure" forceEnable="false" />
		</Dependencies>
		<Files>
			<File name="WarBoard_TogglerPure.lua" />
			<File name="WarBoard_TogglerPure.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="WarBoard_TogglerPure.Initialize" />
		</OnInitialize>
		<WARInfo>
	<Categories>
		<Category name="SYSTEM" />
		<Category name="DEVELOPMENT" />
		<Category name="OTHER" />
	</Categories>
	<Careers>
		<Career name="BLACKGUARD" />
		<Career name="WITCH_ELF" />
		<Career name="DISCIPLE" />
		<Career name="SORCERER" />
		<Career name="IRON_BREAKER" />
		<Career name="SLAYER" />
		<Career name="RUNE_PRIEST" />
		<Career name="ENGINEER" />
		<Career name="BLACK_ORC" />
		<Career name="CHOPPA" />
		<Career name="SHAMAN" />
		<Career name="SQUIG_HERDER" />
		<Career name="WITCH_HUNTER" />
		<Career name="KNIGHT" />
		<Career name="BRIGHT_WIZARD" />
		<Career name="WARRIOR_PRIEST" />
		<Career name="CHOSEN" />
		<Career name= "MARAUDER" />
		<Career name="ZEALOT" />
		<Career name="MAGUS" />
		<Career name="SWORDMASTER" />
		<Career name="SHADOW_WARRIOR" />
		<Career name="WHITE_LION" />
		<Career name="ARCHMAGE" />
	</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>